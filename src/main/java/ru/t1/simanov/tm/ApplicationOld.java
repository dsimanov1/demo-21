package ru.t1.simanov.tm;

public class ApplicationOld {

    public interface Listener {

        void listen(String type, String message);

    }

    public static void main(final String[] args) {
        final Listener listener = new Listener() {
            @Override
            public void listen(String type, String message) {
                System.out.println(type + ":" + message);
            }
        };
        listener.listen("test","HELLO");
        listener.listen("admin", "WORLD");
    }

}
