package ru.t1.simanov.tm.se;

/**
 * @author dsimanov
 */

public class ApplicationNew {

    @FunctionalInterface
    public interface Listener {

        void listen(String type, String message);

    }

    public static void main(String[] args) {
        final Listener listener = (final String t, final String m) -> System.out.println(t + ":" + m);
        listener.listen("test", "HELLO");
        listener.listen("admin", "WORLD");
    }

}
